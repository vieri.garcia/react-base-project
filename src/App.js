
import './App.css';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom'
import { ThemeProvider } from '@mui/material/styles';
import RequireAuth from './components/core/RequireAuth';
import LoginPage from './pages/LoginPage';
import CrudPage from './pages/CrudPage';
import theme from './themeCongif'

function App() {
  return (
    <ThemeProvider theme={theme}>
      <Router>
        <Routes>
          <Route path="/home" element={<RequireAuth><CrudPage></CrudPage></RequireAuth>} />
          <Route path="/" element={<RequireAuth><LoginPage /></RequireAuth>} />
        </Routes>
      </Router>
    </ThemeProvider>

  );
}

export default App;
