import React from 'react'
import {
    Stack,
    Snackbar

} from '@mui/material';
import MuiAlert from '@mui/material/Alert';



const AlertMUI = React.forwardRef(function AlertMUI(props, ref) {
    return <MuiAlert elevation={6} ref={ref} variant="filled" {...props} />;
  });

const Alert = ({severity='success', message, duration=3000}) => {

    const [open, setOpen] = React.useState(false);
    const vertical='top'
    const horizontal= 'right'

    React.useEffect(() => {
        if (message) setOpen(true)
    }, [message])
    

    const handleClose = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }
        setOpen(false);
    };


    return (
        <Stack spacing={2} sx={{ width: '100%' }}>
            <Snackbar open={open} autoHideDuration={duration} onClose={handleClose}anchorOrigin={{ vertical, horizontal }}>
                <AlertMUI onClose={handleClose} severity={severity} sx={{ width: '100%' }}>
                    {message}
                </AlertMUI>
            </Snackbar>
        </Stack>
    );
}

export default Alert
