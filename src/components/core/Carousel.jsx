import React, { useState, useEffect } from "react";
import { isMobile } from 'react-device-detect';

import CarouselMUI from "react-material-ui-carousel";
import {
    Grid,
    CardHeader,
    Card,
    CardContent,
    Typography,
} from "@mui/material";
import { Icon } from "@iconify/react";

import ActionsMenu from "./Crud/ActionsMenu";
import HelpUtils from "../../helpers/HelpUtils";

const Carousel = ({ items = [], frameSize = 6, onClickFrameItem, itemActions, ...props }) => {
    const utils = HelpUtils()
    const noItemsMessage = "No hay registros"

    const [frames, setFrames] = useState([])

    useEffect(() => {
        itemsToFrames()
    }, [items])

    const itemsToFrames = () => {
        setFrames(utils.partList(items, frameSize))
    }


    const Frame = ({ frame = [], index }) => {
        const halves = utils.getListHalves(frame);
        return (
            <Grid
                container
                spacing={0}
                justifyContent="center"
                sx={{
                    p: isMobile ? 1 : 4,
                    overflowY: "auto",
                    height: "100vh",
                }}
                key={index}
            >
                {
                    halves.map((half, index) => (
                        <Grid item xs={12} md={6} elevation={4} key={`half-${index}`}>
                            {half.map((c, i) => (
                                <Card
                                    variant="outlined"
                                    sx={{ backgroundColor: c.cardColor, my: 2, mx: 2 }}
                                    key={c.id}
                                >
                                    <CardHeader
                                        title={
                                            c.cardTitle
                                        }
                                        subheader={
                                            c.cardSubtitle
                                        }
                                        action={
                                            <ActionsMenu
                                                row={c.row}
                                                actionsMenuOnClick={onClickFrameItem}
                                                actions={itemActions}
                                                restrictedActions={c.restrictedActions}
                                            ></ActionsMenu>
                                        }
                                    />
                                    <CardContent>{c.cardContent}</CardContent>
                                </Card>
                            ))}
                        </Grid>

                    ))
                }

            </Grid>
        );
    };

    return (
        <CarouselMUI animation="slide" swipe={isMobile ? false : true} {...props} >
            {frames.length > 0 ?
                frames.map((frame, index) => (
                    <Frame
                        frame={frame}
                        index={index}
                        key={index}
                    />
                )) :
                <Grid container
                    spacing={1}
                    justifyContent="center"
                    direction='row'
                    sx={{
                        p: 4,
                        height: "100vh",
                    }}>
                    <Grid item md={12}>
                        <Typography variant="h4" color="text.secondary" sx={{ flexGrow: 1, textAlign: "center" }}>
                            <Icon
                                icon="iconoir:info-empty"
                                width={120}
                                height={120}
                            />
                            <br />
                            {noItemsMessage}
                        </Typography>
                    </Grid>
                </Grid>
            }
        </CarouselMUI>
    );
}

export default Carousel


