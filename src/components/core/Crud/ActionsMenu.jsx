import React from "react";
import { Icon } from "@iconify/react";
import { useRef, useState } from "react";
// material
import {
  Menu,
  MenuItem,
  IconButton,
  ListItemIcon,
  ListItemText,
} from "@mui/material";


const ActionsMenu = ({ actionsMenuOnClick, actions=[], restrictedActions = [], row = undefined }) => {
  const ref = useRef(null);
  const [isOpen, setIsOpen] = useState(false);

  const handleOnClick = (params) => {
    actionsMenuOnClick({ ...params, row: row });
    setIsOpen(false);
  };

  return (
    <>
      <IconButton ref={ref} onClick={() => setIsOpen(true)}>
        <Icon icon="eva:more-vertical-fill" width={20} height={20} />
      </IconButton>

      <Menu
        open={isOpen}
        anchorEl={ref.current}
        onClose={() => setIsOpen(false)}
        PaperProps={{
          sx: { width: 250, maxWidth: "100%" },
        }}
        anchorOrigin={{ vertical: "top", horizontal: "right" }}
        transformOrigin={{ vertical: "top", horizontal: "right" }}
      >
        {actions ? actions.map((a) => (
          restrictedActions.find(ra => ra === a.action_name) ?
            null
            : <MenuItem
              key={a.action_name}
              onClick={(e) =>
                handleOnClick({ id: row.id, action: a.action_name })
              }
              sx={{ color: "text.secondary" }}
            >
              <ListItemIcon>
                <Icon icon={a.icon} width={24} height={24} />
              </ListItemIcon>
              <ListItemText
                primary={a.action_label}
                primaryTypographyProps={{ variant: "body2" }}
              />
            </MenuItem>
        )) : <></>}
      </Menu>
    </>
  );
};

export default ActionsMenu;
