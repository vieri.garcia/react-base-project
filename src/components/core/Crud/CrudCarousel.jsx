import { useEffect, useState } from 'react'
import HelpConstVariables from '../../../helpers/HelpConstVariables';
import HelpUtils from '../../../helpers/HelpUtils';
import Carousel from '../Carousel'
import Searcher from '../Searcher'


const constVariables = HelpConstVariables()
const defaultActions = constVariables.defaultActions;

const CrudCarousel = ({ rows = [], columns, rowsToItems, frameSize = 6, itemActions = defaultActions, onClickFrameItem, ...props }) => {


    const [items, setItems] = useState([]);

    const [carousel, setCarousel] = useState(undefined)
    const [filterText, setFilterText] = useState("")
    const [filters, setFilters] = useState([])
    const [filteredRows, setFilteredRows] = useState([])


    const searchPlaceholder = "Buscar ..."

    useEffect(() => {
        setItems(rowsToItems(filteredRows))
    }, [filteredRows])

    useEffect(() => {
        setFilters(columns.map((c) => {
            return c.searchable !== undefined && c.searchable && c.field
        }))
    }, [columns])

    useEffect(() => {
        setFilteredRows(rows)
    }, [rows])

    useEffect(() => {
        setCarousel(renderCarousel())
    }, [items])

    const filterItems = () => {
        console.log(filterText)
        let filtered = rows.filter((row) => {
            let response = false
            filters.forEach((filter) => {
                let match = row[filter] ? String(row[filter]).toLocaleLowerCase().includes(filterText.toLocaleLowerCase()) : false
                response = match ? true : response
            });
            return response;
        })
        setFilteredRows(filtered)
    }

    const onChangeFilterValue = (event) => {
        let text = event.target.value
        setFilterText(text)
        if (text !==""){
            filterItems()
        }else{
            setFilteredRows(rows)
        }
    }

    const renderCarousel = () => {
        return (
            <Carousel
                items={items}
                frameSize={frameSize}
                itemActions={itemActions}
                onClickFrameItem={onClickFrameItem}
                {...props}
            />
        )
    }




    return (
        <>
            <Searcher
                filterValue={filterText}
                onChangeFilterValue={onChangeFilterValue}
                placeholder={searchPlaceholder}
            />
            {carousel}
        </>
    )
}

export default CrudCarousel