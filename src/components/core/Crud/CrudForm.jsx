import { useState, useEffect } from 'react'
import {
    isMobile,
} from "react-device-detect";
import {
    Typography,
    Button,
    Modal, Grid,
} from "@mui/material";
import Form from '../Form/Form';



const CrudForm = ({ openModal, setOpenModal, title, fields, formContexMethods, onSubmit, buttonLabel, ...props }) => {


    const [form, setForm] = useState()
    const modalStyle = {
        position: "fixed",
        top: "50%",
        left: "50%",
        height: isMobile ? "75%" : "80vh",
        display: "block",
        transform: "translate(-50%, -50%)",
        width: 400,
        overflowY: "auto",
        overflowX: "auto",
        bgcolor: "background.paper",
        boxShadow: 24,
        p: 4,
        m: 1,
    };


    useEffect(() => {
        setForm(renderForm())
    }, [fields])


    const handleCloseModal = () => {
        setOpenModal(false);
        formContexMethods.reset();
    };

    const renderForm = () => {
        return (
            <Form
                sx={modalStyle}
                onSubmit={onSubmit}
                title={title}
                fields={fields}
                formContexMethods={formContexMethods}
                {...props}
            >
                <Button
                    fullWidth
                    variant="contained"
                    type="submit"
                    sx={{ mt: 3, mb: 2 }}
                >
                    <Typography variant="subtitle2" color="initial">
                        {buttonLabel}
                    </Typography>
                </Button>
            </Form>
        )
    }

    return (
        <Modal
            open={openModal}
            onClose={handleCloseModal}
            aria-labelledby="modal-modal-title"
            aria-describedby="modal-modal-description"
        >
            <Grid container spacing={0}>
                {form}
            </Grid>
        </Modal>
    )
}

export default CrudForm