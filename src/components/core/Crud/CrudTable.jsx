import React, { useEffect, useState } from 'react'
import HelpConstVariables from '../../../helpers/HelpConstVariables';
import Table from '../Table';
import ActionsMenu from './ActionsMenu';


const constVariables = HelpConstVariables()
const defaultActions = constVariables.defaultActions;

const CrudTable = ({rows, columns, actions = defaultActions, actionsMenuOnClick, ...props }) => {

    const [table, setTable] = useState(undefined)

    useEffect(() => {
        addActionsColumn()
    }, [])

    useEffect(() => {
        setTable(renderTable())
    }, [rows])

    useEffect(() => {
        addActionsColumn()
        setTable(renderTable())
    }, [columns])
    
    const addActionsColumn = () => {
        columns.unshift({
            field: "actions",
            minWidth: 50,
            headerName: "",
            filtrable: false,
            type: "actions",
            renderCell: (params) => (
                <strong>           
                    <ActionsMenu
                        rowId={params.row.id}
                        row={params.row}
                        actionsMenuOnClick={actionsMenuOnClick}
                        actions={actions}   
                    ></ActionsMenu>
                </strong>
            ),
        })
    }

    const renderTable = () => {
        return (
            <Table
                rows={rows}
                columns={columns}
                {...props}
            />
        )
    }


    return (
        <>
            {table}
        </>
    )
}

export default CrudTable