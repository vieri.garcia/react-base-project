import React from 'react'

import { Draggable } from 'react-beautiful-dnd';
import ListItem from '@material-ui/core/ListItem';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import ListItemText from '@material-ui/core/ListItemText';
import { Icon } from "@iconify/react";



const DraggableListItem = ({ item, index }) => {
    return (
        <Draggable draggableId={item.id} index={index}>
            {(provided, snapshot) => (
                <ListItem
                    ref={provided.innerRef}
                    {...provided.draggableProps}
                    {...provided.dragHandleProps}
                    sx={{
                        backgroundColor: snapshot.isDragging ? 'rgb(235,235,235)' : ''
                    }}
                >
                    <ListItemAvatar>
                        <Icon icon={item.icon} width={30} height={30} />
                    </ListItemAvatar>
                    <ListItemText primary={item.primary} secondary={item.secondary} />
                </ListItem>
            )}
        </Draggable>
    );
}

export default DraggableListItem

