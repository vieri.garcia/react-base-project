import React from 'react'
import {
    IconButton,
    InputAdornment,
    Tooltip,
    TextField
} from "@mui/material";
import {
    Controller,
} from "react-hook-form";
import ColorLensTwoToneIcon from "@mui/icons-material/Palette";
import { ChromePicker } from "react-color";
import HelpUtils from '../../../helpers/HelpUtils';

const ColorField = ({ field, formContexMethods }) => {

    const [displayColorPicker, setDisplayColorPicker] = React.useState(false);

    const utils = HelpUtils()

    const onChangeColor = (params) => {
        formContexMethods.setValue(field.form_field, params.hex);
    };
    const handleClick = () => {
        setDisplayColorPicker(!displayColorPicker);
    };

    const handleClose = () => {
        setDisplayColorPicker(false);
    };

    const popover = {
        position: "absolute",
        zIndex: "2",
    };
    const cover = {
        position: "fixed",
        top: "0px",
        right: "0px",
        bottom: "0px",
        left: "0px",
    };

    return (
        <Controller
            control={formContexMethods.control}
            name={field.form_field}
            render={({
                field: { onChange, onBlur, value, name, ref },
                fieldState: { invalid, isTouched, isDirty, error },
                formState,
            }) => (
                <>
                    <TextField

                        margin="normal"
                        fullWidth
                        {...field.form_props}
                        id={field.form_field}
                        label={field.form_label}
                        value={value}
                        disabled={field.form_enable !== undefined ? !field.form_enable : false}
                        required={
                            field.form_required !== undefined
                                ? field.form_required
                                : false
                        }
                        InputProps={{
                            startAdornment: (
                                <InputAdornment position="start">
                                    <Tooltip title={field.form_label} placement="bottom">
                                        <IconButton
                                            size="small"
                                            sx={{ backgroundColor: value }}
                                            onClick={handleClick}
                                            disabled={field.form_enable !== undefined ? !field.form_enable : false}
                                        >
                                            <ColorLensTwoToneIcon
                                                sx={{
                                                    color: utils.hexToGrayScale(
                                                        utils.invertHexColor(value||'#FFFFFF')
                                                    )
                                                }}
                                            />
                                        </IconButton>
                                    </Tooltip>
                                </InputAdornment>
                            ),
                        }}
                    />
                    {displayColorPicker ? (
                        <div style={popover}>
                            <div style={cover} onClick={handleClose} />
                            <ChromePicker
                                color={value||'#FFFFFF'}
                                onChangeComplete={onChangeColor}
                                {...field.form_props}
                            />
                        </div>
                    ) : null}
                </>

            )}
        />

    )


}

export default ColorField