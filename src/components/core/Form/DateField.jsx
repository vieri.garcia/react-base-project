
import React from 'react'
import {
    BrowserView,
    MobileView
} from "react-device-detect";
import {
    TextField,
    FormControlLabel,
    FormControl,
} from "@mui/material";
import { LocalizationProvider, DatePicker, MobileDatePicker } from "@mui/lab";
import AdapterDateFns from "@mui/lab/AdapterDateFns";
import {
    Controller,
} from "react-hook-form";


const DateField = ({ field, formContexMethods }) => {

    let props = {
        multiline: field.form_multiline ? true : false,
        rows: field.form_multiline ? field.form_multiline : 1

    }
    const [currentValues, setCurrentValues] = React.useState(formContexMethods.control._formValues)

    const TextInput = (params) => {
        return (
            <TextField
                fullWidth
                margin="normal"
                {...params}
                
                required={
                    field.form_required !== undefined
                        ? field.form_required
                        : false
                }
                error={
                    formContexMethods.formState.errors[field.form_field] ===
                        undefined
                        ? false
                        : true
                }
                helperText={
                    formContexMethods.formState.errors[field.form_field] &&
                    formContexMethods.formState.errors[field.form_field].message
                }
                {...formContexMethods.register(field.form_field)}
            />
        )
    }

    return (
        <Controller
            control={formContexMethods.control}
            name={field.form_field}
            render={({
                field: { onChange, onBlur, value, name, ref },
                fieldState: { invalid, isTouched, isDirty, error },
                formState,
            }) => (
                <LocalizationProvider dateAdapter={AdapterDateFns}>
                    <MobileView>
                        <MobileDatePicker
                            label={field.form_label}
                            fullWidth
                            margin="normal"
                            value={value}
                            onChange={(date) => {
                                onChange(date)
                            }}
                            disabled={field.form_enable !== undefined ? !field.form_enable : false}
                            renderInput={(params) =>TextInput(params) }
                            {...field.form_props}
                        />
                    </MobileView>
                    <BrowserView >
                        <DatePicker
                            disableFuture={field.form_disabled_future ? field.form_disabled_future : false}
                            label={field.form_label}
                            openTo="day"
                            value={value}
                            onChange={(date) => {
                                onChange(date)
                            }}
                            views={["year", "month", "day"]}
                            disabled={field.form_enable !== undefined ? !field.form_enable : false}
                            renderInput={(params) =>TextInput(params)}
                            {...field.form_props}
                        />
                    </BrowserView>
                </LocalizationProvider>
            )}
        />

    )
}

export default DateField