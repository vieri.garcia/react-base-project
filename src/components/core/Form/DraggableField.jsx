import React from 'react'
import {
    TextField,
    Paper, FormControl, FormLabel, FormHelperText, Typography
} from "@mui/material";

import {
    Controller,
    useWatch
} from "react-hook-form";

import DraggableList from '../Draggable/DraggableList';
import HelpUtils from '../../../helpers/HelpUtils'


const DraggableField = ({ field, formContexMethods }) => {

    const [items, setItems] = React.useState([]);

    const utils = HelpUtils();

    //const [currentValues, setCurrentValues] = React.useState(formContexMethods.control._formValues)

    const watchField = useWatch({control:formContexMethods.control,name:field.form_field});
    
    React.useEffect(() => {    
        let currentValue= watchField
        if ( currentValue!='' && field.form_options) {
            setItems(utils.orderArrayByString(field.form_options, currentValue, '|', 'id'))
        }
    }, [watchField])


    return (
        <Controller
            control={formContexMethods.control}
            name={field.form_field}
            render={({
                field: { onChange, onBlur, value, name, ref },
                fieldState: { invalid, isTouched, isDirty, error },
                formState,
            }) => (
                <>
                    <FormControl sx={{ width: '100%' }} {...field.form_props}>
                        <FormLabel sx={{ m: 1 }}>
                            <Typography variant="subtitle2" color="grey" align='left'>{field.form_label}</Typography>
                        </FormLabel>
                        <Paper sx={{ width: '100%' }}>
                            <DraggableList
                                items={items}
                                onDragEnd={
                                    ({ destination, source }) => {
                                        // dropped outside the list
                                        if (!destination) return;
                                        const newItems = utils.reorder(items, source.index, destination.index);
                                        setItems(newItems);
                                        onChange(utils.jsonArrayToString(newItems, '|', 'id'))
                                    }
                                }
                                isDropDisabled={field.form_enable !== undefined ? !field.form_enable : false}
                                {...field.form_props}
                            />
                        </Paper>
                    </FormControl>
                    <TextField
                        margin="normal"
                        required={field.form_no_required !== undefined ? !field.form_no_required : true}

                        fullWidth
                        id={field.form_field}
                        error={formContexMethods.formState.errors[field.form_field] === undefined ? false : true}
                        helperText={formContexMethods.formState.errors[field.form_field] && formContexMethods.formState.errors[field.form_field].message}
                        name={field.form_field}
                        sx={{
                            display: 'none'
                        }}
                        {...formContexMethods.register(field.form_field)}
                    />
                </>
            )}
        />

    )
}

export default DraggableField