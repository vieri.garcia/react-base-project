
import React from 'react'
import PropTypes from 'prop-types'

import {
    FormProvider,
} from "react-hook-form";

import {
    Grid,
    Box,
    Typography
} from '@mui/material'

import TextField from './TextField';
import SelectField from './SelectField'
import SwitchField from './SwitchField';
import ColorField from './ColorField';
import DateField from './DateField';
import IconField from './IconField';
import DraggableField from './DraggableField';
import SliderField from './SliderField';

const Form = ({title,  fields, formContexMethods, onSubmit, children, ...props }) => {


    const generateFields = () => {

        const Field = ({ field, data }) => {
            switch (field.form_field_type) {
                case 'text':
                    return (
                        <TextField
                            field={field}
                            formContexMethods={formContexMethods}
                        />
                    )
                case 'select':
                    return (
                        <SelectField
                            field={field}
                            formContexMethods={formContexMethods}
                        />
                    )
                case 'switch':
                    return (
                        <SwitchField
                            field={field}
                            formContexMethods={formContexMethods}
                        />
                    )
                case 'color':
                    return (
                        <ColorField
                            field={field}
                            formContexMethods={formContexMethods}
                        />
                    )
                case 'date':
                    return (
                        <DateField
                            field={field}
                            formContexMethods={formContexMethods}
                        />
                    )
                case 'icon':
                    return (
                        <IconField
                            field={field}
                            formContexMethods={formContexMethods}
                        />
                    )
                case 'draggable':
                    return (
                        <DraggableField
                            field={field}
                            formContexMethods={formContexMethods}
                        />
                    )
                case 'slider':
                    return (
                        <SliderField
                            field={field}
                            formContexMethods={formContexMethods}
                        />
                    )
                default:
                    return (
                        <TextField
                            field={field}
                            formContexMethods={formContexMethods}
                        />
                    )
            }

        }

        return (
            <Grid container>
                {
                    fields !== undefined &&
                    fields.map((field, index) => (
                        <Grid
                            item
                            xs={12}
                            md={12}
                            key={field.form_field}
                        >
                            {field.form_hide === undefined || !field.form_hide ?
                                <Field
                                    field={field}
                                /> : null
                            }

                        </Grid>
                    ))
                }
            </Grid>

        )
    }

    return (
        <FormProvider {...formContexMethods}>
            <Box
                component="form"
                noValidate
                onSubmit={formContexMethods.handleSubmit(onSubmit)}
                {...props}
            >
                <Grid container spacing={1} justifyContent="center">
                    <Grid
                        item
                        xs={10}
                        md={10}
                    >
                        <Typography
                            component="h1"
                            variant="h5"
                            sx={{ textAlign: "center" }}
                        >
                           {title}
                        </Typography>

                    </Grid>

                    <Grid
                        item
                        xs={10}
                        md={10}
                    >
                        {generateFields()}
                    </Grid>
                    <Grid
                        item
                        xs={10}
                        md={10}
                    >
                        {children}

                    </Grid>

                </Grid>
            </Box>
        </FormProvider>
    )
}

Form.propTypes = {
    fields: PropTypes.array,
    data: PropTypes.object
}

export default Form