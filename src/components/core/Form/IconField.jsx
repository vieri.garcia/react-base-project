import React from 'react'
import {
    Grid,
    TextField,
    IconButton,
    InputAdornment,
    Tooltip,
    Paper,
    Typography,
} from "@mui/material";
import {
    Controller,
} from "react-hook-form";
import { Icon } from "@iconify/react";

const IconList = ({ setIconPickerValue }) => {

    const no_match_message = "No hay coincidencias"

    const icons = ['ic:baseline-adb','ic:baseline-account-balance']
    const [filterIcons, setFilterIcons] = React.useState(icons);
    const [searchText, setSearchText] = React.useState("");
    

    React.useEffect(() => {
        /*handleTypeRequests({
            type_code: "ICON",
            search_text: searchText,
            total: 30,
        }).then((res) => {
            setFilterIcons(res.map((i) => i.description));
        });*/
        console.log(searchText)
    }, [searchText]);

    const onChange = (e) => {
        setSearchText(e.target.value);
    };
    const onClick = (icon) => {
        setIconPickerValue(icon);
    };


    return (
        <Grid
            container
            spacing={0}
            component={Paper}
            justifyContent="center"
            sx={{
                position: "absolute",
                height: "220px",
                width: "200px",
                background: "#fcfcfc",
                textAlign: "center",
                boxShadow:
                    "0 16px 24px 2px rgba(0, 0, 0, 0.14), 0 6px 30px 5px rgba(0, 0, 0, 0.12), 0 8px 10px -5px rgba(0, 0, 0, 0.3)",
                p: "20px",
                overflowY: "auto",
            }}
        >
            <Grid item md={12} sx={{ m: 1 }}>
                <TextField
                    id="icon_selected"
                    fullWidth
                    label=""
                    value={searchText}
                    variant="standard"
                    onChange={onChange}
                />
            </Grid>
            <Grid item md={12} sx={{ m: 1 }}>
                <Grid container spacing={0} justifyContent="center">
                    {filterIcons
                        ? filterIcons.length > 0 ? filterIcons.map((icon) => (
                            <Grid item md={4} key={icon}>
                                <Tooltip title={icon}>
                                    <Icon
                                        icon={icon}
                                        width={30}
                                        height={30}
                                        onClick={() => onClick(icon)}
                                    />
                                </Tooltip>
                            </Grid>
                        )) :
                            <>

                                <Typography variant="subtitle2" color="text.secondary">
                                    <Icon
                                        icon="iconoir:info-empty"
                                        width={30}
                                        height={30}
                                    />
                                    <br />
                                    {no_match_message}
                                </Typography>
                            </>
                        : null
                    }
                </Grid>
            </Grid>
        </Grid>
    );
};

const IconField = ({ field, formContexMethods }) => {

    const [currentIcon, setCurrentIcon] = React.useState("");

    const [displayIconPicker, setDisplayIconPicker] = React.useState(false);

    const onChangeIcon = (icon) => {
        formContexMethods.setValue(field.form_field, icon);
        setCurrentIcon(icon);
    };

    const handleClick = () => {
        setDisplayIconPicker(!displayIconPicker);
    };

    const handleClose = () => {
        setDisplayIconPicker(false);
    };

    const popover = {
        position: "absolute",
        zIndex: "2",
    };
    const cover = {
        position: "fixed",
        top: "0px",
        right: "0px",
        bottom: "0px",
        left: "0px",
    };

    const renderIconPicker = () => {
        return (
            <IconList text={currentIcon} setIconPickerValue={onChangeIcon} />
        );
    };




    //const [currentValues, setCurrentValues] = React.useState(formContexMethods.control._formValues)

    return (
        <Controller
            control={formContexMethods.control}
            name={field.form_field}
            render={({
                field: { onChange, onBlur, value, name, ref },
                fieldState: { invalid, isTouched, isDirty, error },
                formState,
            }) => (
                <>
                    <TextField
                        margin="normal"
                        disabled={field.form_enable !== undefined ? !field.form_enable : false}
                        fullWidth
                        label={field.form_label}
                        error={
                            formContexMethods.formState.errors[field.form_field] === undefined
                                ? false
                                : true
                        }
                        helperText={
                            formContexMethods.formState.errors[field.form_field] &&
                            formContexMethods.formState.errors[field.form_field].message
                        }
                        name={field.form_field}
                        InputProps={{
                            startAdornment: (
                                <InputAdornment position="start">
                                    <Tooltip title={field.form_label} placement="bottom">
                                        <IconButton size="small" onClick={handleClick}>
                                            <Icon icon={value} width={30} height={30} />
                                        </IconButton>
                                    </Tooltip>
                                </InputAdornment>
                            ),
                        }}
                        {...field.form_props}
                        {...formContexMethods.register(field.form_field)}
                    />
                    {displayIconPicker ? (
                        <div style={popover}>
                            <div style={cover} onClick={handleClose} />
                            {renderIconPicker()}
                        </div>
                    ) : null}
                </>
            )}
        />

    )
}

export default IconField


