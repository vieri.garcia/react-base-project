import React from 'react'
import {
    Box,
    TextField,
    Autocomplete
} from "@mui/material";

import {
    Controller,
    useWatch
} from "react-hook-form";

const SelectField = ({ field, formContexMethods }) => {

    const id_attr = field.form_option_id_attr || 'id'
    const description_attr = field.form_option_description_attr || 'description'

    const [options, setOptions] = React.useState(field.form_options)

    let props = new Object()
    if (field.form_render_options) props.renderOption = field.form_render_options

    //const [currentValues, setCurrentValues] = React.useState(formContexMethods.control._formValues)


    const watchField = useWatch({control:formContexMethods.control,name:field.form_watched_field});
    
    React.useEffect(() => {    
        if (field.form_watched_field !== undefined) {
            let currentWatchedValue =watchField
            setOptions(field.form_watched_action(currentWatchedValue))
           
        }
    }, [watchField])


    return (
        <Controller
            name={field.form_field}
            control={formContexMethods.control}
            render={({
                field: { onChange, onBlur, value, name, ref },
                fieldState: { invalid, isTouched, isDirty, error },
                formState,
            }) => (
                <Autocomplete
                    {...props}
                    {...field.form_props}
                    id={field.form_field}
                    options={options}
                    getOptionLabel={(option) => option[description_attr]}
                    disabled={field.form_enable !== undefined ? !field.form_enable : false}
                    /*isOptionEqualToValue={(option, value) => {
                        return option[id_attr] === value[id_attr]
                    }}*/
                    renderInput={(params) => (
                        <TextField
                            {...params}
                            margin="normal"
                            label={field.form_label}
                            fullWidth
                            required={field.form_no_required !== undefined ? !field.form_no_required : true}
                            error={formContexMethods.formState.errors[field.form_field] === undefined ? false : true}
                            helperText={formContexMethods.formState.errors[field.form_field] && formContexMethods.formState.errors[field.form_field].message}

                        />
                    )}
                    value={options.find((opt) => opt[id_attr] == value)}
                    onChange={(_, item) => {
                        onChange(item ? item[id_attr] : '');
                    }}
                    
                />
            )}

        />
    )
}

export default SelectField