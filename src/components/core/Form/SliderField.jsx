import React from 'react'
import {
    Slider,
    FormControlLabel,
    FormControl,
    Box,
    Stack
} from "@mui/material";
import {
    Controller,
} from "react-hook-form";


const SliderField = ({ field, formContexMethods }) => {

    const [currentValues, setCurrentValues] = React.useState(formContexMethods.control._formValues)

    return (
        <Controller
            control={formContexMethods.control}
            name={field.form_field}
           
            render={({
                field: { onChange, onBlur, value, name, ref },
                fieldState: { invalid, isTouched, isDirty, error },
                formState,
            }) => (
                <FormControl component="fieldset" variant="standard" sx={{ width: '100%' }} {...field.form_props}>
                    <FormControlLabel
                        control={
                            <Box sx={{m:2, width:  '100%' }}>
                                <Stack spacing={2} direction="row" sx={{ mb: 1 }} alignItems="center">
                                    <Slider
                                        value={value}
                                        onChange={(_, value) => {
                                            onChange(value);
                                        }}
                                        valueLabelDisplay="auto"
                                        disabled={field.form_enable!==undefined?!field.form_enable:false }
                                        {...field.form_props}
                                    />
                                </Stack>
                            </Box>

                        }
                        label={field.form_label}
                    />
                </FormControl>
            )}
        />
    )
}

export default SliderField