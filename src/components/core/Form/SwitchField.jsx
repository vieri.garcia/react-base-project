import React from 'react'
import {
    Switch,
    FormControlLabel,
    FormControl,
} from "@mui/material";
import {
    Controller,
} from "react-hook-form";


const SwitchField = ({ field, formContexMethods }) => {

    //const [currentValues, setCurrentValues] = React.useState(formContexMethods.control._formValues)

    return (
        <Controller
            control={formContexMethods.control}
            name={field.form_field}
            render={({
                field: { onChange, onBlur, value, name, ref },
                fieldState: { invalid, isTouched, isDirty, error },
                formState,
            }) => (
                <FormControl component="fieldset" variant="standard" {...field.form_props}>
                    <FormControlLabel
                        control={
                            <Switch
                                key={field.form_field}
                                checked={value}
                                disabled={field.form_enable!==undefined?!field.form_enable:false }
                                onChange={(_, target) => {
                                    onChange( target);
                                }}
                                
                            />
                        }
                        label={field.form_label}
                    />
                </FormControl>
            )}
        />

    )
}

export default SwitchField