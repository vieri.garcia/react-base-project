import React from 'react'
import TextFieldMUI from '@mui/material/TextField'
import {
    InputAdornment
} from "@mui/material";
import {
    Controller,
} from "react-hook-form";


const TextField = ({ field,  formContexMethods }) => {

    let props = {
        multiline: field.form_multiline ? true : false,
        rows: field.form_multiline ? field.form_multiline : 1

    }
    
    const [currentValues, setCurrentValues] = React.useState(formContexMethods.control._formValues)

    return (
        <Controller
            control={formContexMethods.control}
            name={field.form_field}
            render={({
                field: { onChange, onBlur, value, name, ref },
                fieldState: { invalid, isTouched, isDirty, error },
                formState,
            }) => (
                <TextFieldMUI
                    {...props}
                    margin="normal"
                    fullWidth
                    {...field.form_props}
                    id={field.form_field}
                    label={field.form_label}
                    value={value}
                    disabled={field.form_enable!==undefined?!field.form_enable:false }
                    required={
                        field.form_required !== undefined
                            ? field.form_required
                            : false
                    }
                    type={field.form_field_data_type}
                    error={
                        formContexMethods.formState.errors[field.form_field] ===
                            undefined
                            ? false
                            : true
                    }
                    helperText={
                        formContexMethods.formState.errors[field.form_field] &&
                        formContexMethods.formState.errors[field.form_field].message
                    }
                    InputProps={{
                        startAdornment: (
                            <InputAdornment position="start">
                                {currentValues[field.form_input_adorment_field]}
                            </InputAdornment>
                        ),
                    }}
                    
                    {...formContexMethods.register(field.form_field)}
                />
            )}
        />

    )
}

export default TextField