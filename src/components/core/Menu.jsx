import React from 'react'
import { isMobile } from 'react-device-detect';
// Material UI
import {
    Avatar,
    List,
    ListSubheader,
    Divider,
    ListItem,
    ListItemIcon,
    ListItemText,
    Grid,
    Typography
} from '@mui/material';
import { Icon } from "@iconify/react";

const Menu = ({ menuOptions, listTitle, hasHeader = true, hasAvatar = true }) => {

    const [selectedIndex, setSelectedIndex] = React.useState(0);

    const handleListItemClick = (index, onClickFunct, onClickParam) => {
        setSelectedIndex(index);
        onClickParam ? onClickFunct(onClickParam) : onClickFunct()
    };

    const stringToColor = (string) => {
        let hash = 0;
        let i;

        /* eslint-disable no-bitwise */
        for (i = 0; i < string.length; i += 1) {
            hash = string.charCodeAt(i) + ((hash << 5) - hash);
        }

        let color = '#';

        for (i = 0; i < 3; i += 1) {
            const value = (hash >> (i * 8)) & 0xff;
            color += `00${value.toString(16)}`.substr(-2);
        }
        /* eslint-enable no-bitwise */

        return color;
    }

    const stringAvatar = (name) => {
        let dimention = isMobile ? 62 : 80
        return {
            sx: {
                bgcolor: stringToColor(name),
                width: dimention,
                height: dimention
            },
            children: `${name.split(' ')[0][0]}${name.split(' ')[1][0]}`,
        };
    }

    return (
        <List sx={{ p: 1, width: '100%' }}>
            {hasHeader &&
                <>
                    <ListSubheader>
                        <Grid
                            container
                            spacing={1}
                            justifyContent="center"
                            direction="row"
                            sx={{
                                p: 1
                            }}
                        >
                            {hasAvatar &&
                                <Grid item xs={12} md={12} sx={{ display: 'flex', alignItems: 'center', justifyContent: 'center' }} >
                                    <Avatar {...stringAvatar(listTitle)} />
                                </Grid>}

                            <Grid item xs={12} md={12} sx={{ display: 'flex', alignItems: hasAvatar?'center':'left', justifyContent:  hasAvatar?'center':'left' }}>
                                <Typography variant="subtitle1" sx={{ color: hasAvatar?stringToColor(listTitle):'grey', fontWeight: 'bold' }}>{listTitle}</Typography>
                            </Grid>
                        </Grid>
                    </ListSubheader>
                    <Divider></Divider>
                </>
            }
            {menuOptions.map((o, index) => (
                <>
                    <ListItem button key={o.key} onClick={(e) => { handleListItemClick(index, o.onClickFunct, o.onClickParam) }} selected={selectedIndex === index}>
                        {o.icon !== undefined &&
                            <ListItemIcon>
                                <Icon icon={o.icon} width={30} height={30} />
                            </ListItemIcon>}
                        <ListItemText primary={o.description} />
                    </ListItem>
                </>
            ))}
        </List>
    )
}

export default Menu
