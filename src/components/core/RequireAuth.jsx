import React from 'react'
import { Navigate } from 'react-router-dom'
import AuthContext from '../../context/AuthContext';


const RequireAuth= ({ children }) => {
    const [auth,,,,] = React.useContext(AuthContext);
    return auth === true 
      ? children 
      : <Navigate to="/login" />;
  }

export default RequireAuth
