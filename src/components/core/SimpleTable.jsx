import React from 'react'
import {
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableHead,
    TableRow,
    Paper,
    Grid
} from '@mui/material';
import HelpUtils from '../../helpers/HelpUtils';

const SimpleTable = ({ columns = [], rows = [], total, is_currency = false, currency_symbol }) => {

    //const [total, setTotal] = React.useState(0)
    const utils = HelpUtils()


    const computeTotal = (column_field) => {
        let result = 0;

        if (rows.length > 0) {
            let data = rows.map((row) => row[column_field]);
            if (total.operation === 'SUM') {
                result = (data.reduce((partial_sum, a) => partial_sum + a)).toFixed(2)
            }
        }


        return result

    }


    return (
        <TableContainer component={Paper}>
            <Table sx={{
                width: '100%',
               
            }} size="small" aria-label="simple table">
                <TableHead>
                    <TableRow>
                        {
                            total ?
                                <TableCell key={'total'} />
                                : <></>
                        }
                        {
                            columns.map((column, index) => (
                                <TableCell key={column.field}><strong>{column.headerName}</strong></TableCell>
                            ))
                        }
                    </TableRow>
                </TableHead>
                <TableBody>
                    {rows.map((row, index) => (
                        <TableRow
                            key={row.id}
                            sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                        ><>
                                {total ?
                                    <TableCell width={20} /> : null}
                                {
                                    columns.map((column, index) => (
                                        <>{
                                            column.field !== 'actions' ?
                                                <TableCell key={column.field} width={column.minWidth || 50} component="th" scope="row">
                                                    {row[column.field]}
                                                </TableCell> : null
                                        }
                                        </>

                                    ))
                                }
                            </>

                        </TableRow>
                    ))}
                    {
                        total ?
                            <TableRow key={'total-row'}>
                                <TableCell key={'total-header'} align='right'><strong >{total.headerName}</strong></TableCell>
                                {
                                    columns.map((column, index) => (
                                        <>
                                            {

                                                column.inTotals && column.field != 'actions' ?
                                                    <TableCell key={'total-' + column.field} width={column.minWidth || 50} component="th" scope="row">
                                                        {computeTotal(column.field)}
                                                    </TableCell>
                                                    : <TableCell key={'total-' + column.field} width={column.minWidth || 50} />

                                            }
                                        </>
                                    ))
                                }
                            </TableRow>
                            : null
                    }
                </TableBody>
            </Table>
        </TableContainer>
    )
}

export default SimpleTable
