import React from "react";

const AuthContext = React.createContext();



const AuthProvider = ({ children }) => {


  const [auth, setAuth] = React.useState(true);
  const [token, setToken] = React.useState("");
  const [user, setUser] = React.useState({});

  const handleToken = (token) => {
    if (token && token !== '') {
      localStorage.setItem("token", token);
      setAuth(true)
    } else {
      localStorage.removeItem("token");
      setAuth(false)
    }
    setToken(token)
  }

  const handleUser = (user) => {
    setUser(user)
  }

  const data = [
    auth,
    token,
    handleToken,
    user,
    handleUser
  ];

  return <AuthContext.Provider value={data}>{children}</AuthContext.Provider>;
};

export { AuthProvider };
export default AuthContext;
