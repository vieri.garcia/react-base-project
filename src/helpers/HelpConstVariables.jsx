import React from 'react'

const HelpConstVariables = () => {
  
    const defaultActions = [
        {
            action_name: 'edit',
            action_label: "Editar",
            icon: 'eva:edit-fill'
        },
        {
            action_name: 'delete',
            action_label: "Eliminar",
            icon: 'eva:trash-2-outline'
        }
    ]
    return {defaultActions}
}

export default HelpConstVariables