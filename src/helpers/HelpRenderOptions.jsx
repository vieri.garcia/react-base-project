import React from 'react'
import {  Box } from '@mui/material'

const HelpRenderOptions = () => {

 const flagsRenderOptions = (id_attr, description_attr) => {
    return (props, option) => (
        <Box component="li" sx={{ '& > img': { mr: 2, flexShrink: 0 } }} {...props}>
            <img
                loading="lazy"
                width="20"
                src={`https://flagcdn.com/w20/${option[id_attr].toLowerCase()}.png`}
                srcSet={`https://flagcdn.com/w40/${option[id_attr].toLowerCase()}.png 2x`}
                alt=""
            />
            {option[description_attr]} ({option[id_attr]})
        </Box>
    )
 }
  return {flagsRenderOptions}
}

export default HelpRenderOptions