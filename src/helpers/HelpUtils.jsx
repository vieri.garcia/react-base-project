import React from "react";

const HelpUtils = () => {


  const radomItemFromSet = (set=[]) =>{
    var item = (Math.random()*set.length+1).toFixed(0);
    return set[item];
  }
    
  const randomHexColor = () => {
    const set =["a","b","c","d","e","f","0","1","2","3","4","5","6","7","8","9"]
    var color = "";
    for(var i=0;i<6;i++){
      color = color + radomItemFromSet(set) ;
    }
    return "#" + color;

  }

  const invertHexColor = (hex) => {
    hex = hex.split('#')[1]
    return '#' + (Number(`0x1${hex}`) ^ 0xFFFFFF).toString(16).substr(1).toUpperCase()
  };

  const hexToRgb = (hex) => {
    hex = hex.split('#')[1]
    let bigint = parseInt(hex, 16);
    let r = (bigint >> 16) & 255;
    let g = (bigint >> 8) & 255;
    let b = bigint & 255;
    return [r, g, b]
  }

  const componentToHex = (c) => {
    var hex = c.toString(16);
    return hex.length == 1 ? "0" + hex : hex;
  }

  const rgbToHex = (r, g, b) => {
    return "#" + componentToHex(r) + componentToHex(g) + componentToHex(b);
  }

  const shallowEqual = (object1, object2) => {
    const keys1 = Object.keys(object1);
    const keys2 = Object.keys(object2);
    if (keys1.length !== keys2.length) {
      return false;
    }
    for (let key of keys1) {
      if (object1[key] !== object2[key]) {
        console.log(object1[key], object2[key])
        return false;
      }
    }
    return true;
  }

  const hexToGrayScale = (hex) => {
    let rgb = hexToRgb(hex)
    let x = Math.floor(0.2126 * rgb[0] + 0.7152 * rgb[1] + 0.0722 * rgb[2])
    x = Math.round(x)
    return rgbToHex(x, x, x)
  }

  const getListHalves = (list) => {
    const length = list.length;
    const half = Math.ceil(length / 2);
    const firstHalf = length === 1 ? list : list.slice(0, half);
    const secondHalf = length === 1 ? [] : list.slice(-half + (length % 2));

    return [firstHalf, secondHalf];
  };

  const partList = (list, partSize) => {
    const arrayOfParts = [];
    for (let i = 0; i < list.length; i += partSize) {
      let part = list.slice(i, i + partSize);
      arrayOfParts.push(part);
    }
    return arrayOfParts;
  };

  const currencyFormatter = (currency, sign) => {
    var sansDec = currency;
    var formatted = sansDec.replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    return sign + ` ${formatted}`;
  };

  const findByField = (array, value, field = "description") => {
    return array.find((c) => c[field] === value);
  };

  const orderArrayByString = (array, orderString, separator = '|', field = 'key') => {
    let orderArray = orderString.split(separator)
    return orderArray.map((e) => findByField(array, e, field))
  }

  const jsonArrayToString = (array, separator = '|', field = 'key') => {
    let string = ''
    array.forEach((e) => { string += e[field] + separator })
    return string.slice(0, -1)
  }


  const gcd = (a, b) => {
    return (b) ? gcd(b, a % b) : a;
  }
  const decimalToFraction = (_decimal) => {
    if (_decimal == parseInt(_decimal)) {
      return {
        top: parseInt(_decimal),
        bottom: 1,
        display: parseInt(_decimal) + '/' + 1
      };
    }
    else {
      var top = _decimal.toString().includes(".") ? _decimal.toString().replace(/\d+[.]/, '') : 0;
      var bottom = Math.pow(10, top.toString().replace('-', '').length);
      if (_decimal >= 1) {
        top = +top + (Math.floor(_decimal) * bottom);
      }
      else if (_decimal <= -1) {
        top = +top + (Math.ceil(_decimal) * bottom);
      }

      var x = Math.abs(gcd(top, bottom));
      return {
        top: (top / x),
        bottom: (bottom / x),
        display: (top / x) + '/' + (bottom / x)
      };
    }
  };

  const reorder = (
    list,
    startIndex,
    endIndex
  ) => {
    const result = Array.from(list);
    const [removed] = result.splice(startIndex, 1);
    result.splice(endIndex, 0, removed);

    return result;
  };

  const toIsoString = (date) => {
    var tzo = -date.getTimezoneOffset(),
      dif = tzo >= 0 ? '+' : '-',
      pad = function (num) {
        var norm = Math.floor(Math.abs(num));
        return (norm < 10 ? '0' : '') + norm;
      };

    return date.getFullYear() +
      '-' + pad(date.getMonth() + 1) +
      '-' + pad(date.getDate()) +
      'T' + pad(date.getHours()) +
      ':' + pad(date.getMinutes()) +
      ':' + pad(date.getSeconds()) +
      dif + pad(tzo / 60) +
      ':' + pad(tzo % 60);

  }

  const formatDate = (date) => {
    var d = new Date(date),
      month = '' + (d.getMonth() + 1),
      day = '' + d.getDate(),
      year = d.getFullYear();

    if (month.length < 2)
      month = '0' + month;
    if (day.length < 2)
      day = '0' + day;

    return [year, month, day].join('-');
  }

  const jsonConcat=(o1, o2)=> {
    for (var key in o2) {
      o1[key] = o2[key];
    }
    return o1;
  }
  return {
    currencyFormatter,
    findByField,
    getListHalves,
    partList,
    invertHexColor,
    rgbToHex,
    hexToGrayScale,
    shallowEqual,
    decimalToFraction,
    reorder,
    orderArrayByString,
    jsonArrayToString,
    formatDate,
    toIsoString,
    jsonConcat,
    randomHexColor
  };
};

export default HelpUtils;
