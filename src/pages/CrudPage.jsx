import React, { useEffect, useState } from 'react'
import {
    useForm,
} from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";
import { Typography,  Grid, Stack } from "@mui/material";


import CrudForm from '../components/core/Crud/CrudForm'
import CrudTable from '../components/core/Crud/CrudTable'
import ExampleService from '../services/ExampleService'
import Button from '@mui/material/Button'
import CrudCarousel from '../components/core/Crud/CrudCarousel';
import HelpUtils from '../helpers/HelpUtils'
import SimpleTable from '../components/core/SimpleTable';
import Alert from '../components/core/Alert';

const CrudPage = () => {

    const service = ExampleService()
    const [rows, setRows] = useState([])
    const [selectedRow, setSelectedRow] = useState()
    const [openModal, setOpenModal] = useState(false)
    const [refreshTable, setRefreshTable] = useState(false)
    const [fields, setFields] = useState([])
    const [bottomLabel, setBottomLabel] = useState("Crear")
    const [alert, setAlert] = useState({ message: undefined })


    const columns = [
        {
            field: "name",
            headerName: "Nombre",
            searchable: true
        }, {
            field: "last_name",
            headerName: "Apellidos",
            minWidth: 180,
            searchable: true
        }, {
            field: "age",
            headerName: "Edad",
            type: "number",
            searchable: true,
            inTotals: true
        }
    ]

    const total = {
        'operation': 'SUM',
        'headerName': 'Suma Total',
        'isCurrency': false,
    }

    const validationSchema = yup.object().shape({
        name: yup
            .string()
            .required("Requerido"),
        last_name: yup
            .string()
            .required("Requerido"),
        age: yup
            .number()
            .required("Requerido")
    });


    const utils = HelpUtils()
    const methods = useForm({ resolver: yupResolver(validationSchema) });

    useEffect(() => {
        setFields(configureFields())
        loadData()
    }, [])

    useEffect(() => {
        if (refreshTable) loadData(); setRefreshTable(false)
    }, [refreshTable])


    useEffect(() => {
        if (selectedRow) {
            methods.reset(selectedRow)
            setBottomLabel("Actualizar")
        } else {
            setBottomLabel("Crear")
        }
        setFields(configureFields())
    }, [selectedRow])

    useEffect(() => {
        if (!openModal) {
            setSelectedRow(undefined)
        }
    }, [openModal])


    const configureFields = () => {
        return [
            {
                form_field: "name",
                form_label: "Nombre",
                form_required: true,
                form_field_type: "text",
                form_field_data_type: "text",
                form_hide: false,
            }, {
                form_field: "last_name",
                form_label: "Apellidos",
                form_required: true,
                form_field_type: "text",
                form_hide: false,
            }, {
                form_field: "age",
                form_label: "Edad",
                form_required: true,
                form_field_type: "slider",
                form_props: { 'max': 75, 'min': 18, 'step': 1 },
                form_hide: selectedRow ? true : false,
            }, {

                form_label: "Color Favorito",
                form_field: "color",
                form_field_type: "color"
            }
        ]
    }

    const loadData = () => {
        service.getUsers().then((res) => {
            setRows(res)
        });
    }

    const actionsMenuOnClick = ({ action, id, row }) => {

        switch (action) {
            case 'edit':
                service.getUser(id).then((res) => {
                    setSelectedRow(res)
                    setOpenModal(true)
                })
                break;
            case 'delete':
                service.deleteUser(id).then((res) => {
                    setRefreshTable(true)
                    setAlert({ ...alert, message: `El usuario ${row.name} ${row.last_name} fue eliminado.` })
                })
                break;

        }
    }

    const formOnSubmit = (data) => {
        if (selectedRow) {
            service.updateUser(data.id, data).then((res) => {
                setRefreshTable(true)
                setAlert({ ...alert, message: `El usuario ${data.name} ${data.last_name} fue actualizado.` })
            })
        } else {
            service.createUser(data).then((res) => {
                setRefreshTable(true)
                setAlert({ ...alert, message: `El usuario ${data.name} ${data.last_name} fue creado.` })
            })
        }
        setOpenModal(false)
    }

    const createButtonOnClick = () => {
        methods.reset({})
        setOpenModal(true)
    }

    const rowsToItems = (rows) => {
        return rows.map((row) => {
            return {
                id: row.id,
                cardTitle: (
                    <Stack direction="row" spacing={1}>
                        <Typography
                            variant="h5"
                            color={row.color ? utils.hexToGrayScale(utils.invertHexColor(row.color)) : undefined}
                        >
                            {row.name + " " + row.last_name}
                        </Typography>
                    </Stack>
                ),
                cardSubtitle: (
                    <>
                        <Typography
                            variant="subtitle1"
                            color={row.color ? utils.hexToGrayScale(utils.invertHexColor(row.color)) : undefined}
                        >
                            {row.name + " " + row.last_name}
                        </Typography>
                    </>
                ),
                cardColor: row.color || utils.randomHexColor(),
                restrictedActions: [],
                cardContent: (
                    <Grid
                        container
                        spacing={1}
                        direction="column"
                        justify="flex-start"
                        alignItems="left"
                    >
                        <Typography variant="subtitle1" color="initial">
                            {"Edad:" + row.age}
                        </Typography>
                    </Grid>
                ),
                row: row
            };
        });

    }

    return (
        <>
            <>
                <Button variant="text" color="primary" onClick={createButtonOnClick}>
                    Crear Usuario
                </Button>
                <CrudTable
                    rows={rows}
                    columns={columns}
                    actionsMenuOnClick={actionsMenuOnClick}
                />
            </>
            <CrudForm
                title="Titulo"
                fields={fields}
                formContexMethods={methods}
                onSubmit={formOnSubmit}
                buttonLabel={bottomLabel}
                openModal={openModal}
                setOpenModal={setOpenModal}
            />
            <SimpleTable
                columns={columns}
                rows={rows}
                total={total}
            />
            <CrudCarousel
                rows={rows}
                columns={columns}
                rowsToItems={rowsToItems}
                onClickFrameItem={actionsMenuOnClick}

            />
            <Alert
                message={alert.message}
                severity={alert.severity}
            />
        </>
    )
}

export default CrudPage