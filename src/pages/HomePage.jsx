import React from 'react'
import { AuthService } from '../services/core/AuthService'

const HomePage = () => {

  const authService = AuthService();
  const [user, setUser] = React.useState(authService.getUser(''))
  
  return (
    <div className="App">
    <header className="App-header">
      <img className="App-logo" alt="logo" />
      <p>
          Welcome {user['name']}
      </p>
      <p>
        Edit <code>src/App.js</code> and save to reload.
      </p>
      <a
        className="App-link"
        href="https://reactjs.org"
        target="_blank"
        rel="noopener noreferrer"
      >
        Learn React
      </a>
    </header>
  </div>
  )
}

export default HomePage