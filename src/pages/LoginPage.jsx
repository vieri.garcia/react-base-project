import React from 'react'
import { Typography, Button, Box } from '@mui/material'

import {
    useForm,
    FormProvider,
    useFormContext,
    Controller,
} from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";

import Form from '../components/core/Form/Form'
import ExampleService from '../services/ExampleService';
import HelpRenderOptions from '../helpers/HelpRenderOptions';

const LoginPage = () => {

    const renderOptions = HelpRenderOptions()

    const get_languages = (country_id) => {
        switch (country_id) {
            case 'pe':
                return [{ 'code': 'ES', 'name': 'Español' },
                { 'code': 'QU', 'name': 'Quechua' }]
            case 'us':
                return [{ 'code': 'EN', 'name': 'English', }]
        }
    }

    const data = {
        'timestamp': 1549892280,
        'currency_id': 'pe',
        'currency_symbol': "$",
        'status_id': false,
        'color_hex': '#bb4747',
        'created': "03/31/2022",
        'icon': 'dashicons:money-alt',
        'sorted_menu_opt': 'A|B',
        'slider': 50,
        'language': 'ES'
    }

    const fields = [
        {

            form_field: "timestamp",
            form_label: "Timestamp",
            form_required: true,
            form_field_type: "text",
            form_field_data_type: "number",
            form_show_field: true,
            form_input_adorment_field: "currency_symbol",
            //form_multiline:1,
        }, {
            form_label: "Currency",
            form_field: "currency_id",
            form_enable: true,
            form_field_type: "select",
            form_option_id_attr: 'code',
            form_option_description_attr: 'name',
            form_options: [{ 'code': 'us', 'name': 'American Dollar', 'currency_symbol': '$' },
            { 'code': 'pe', 'name': 'Sol Peruano', 'currency_symbol': 'S/.' }],
            form_render_options: renderOptions.flagsRenderOptions('code', 'name'),
        }, {
            form_label: "Language",
            form_field: "language",
            form_enable: true,
            form_field_type: "select",
            form_option_id_attr: 'code',
            form_option_description_attr: 'name',
            form_options: get_languages(data['currency_id']),
            form_watched_field: 'currency_id',
            form_watched_action: get_languages
        },
        {
            form_label: 'Estado',
            form_field: "status_id",
            form_field_type: "switch",

        }, {

            form_label: "Color",
            form_field: "color_hex",
            form_field_type: "color",
            //form_enable: false,
        }, {
            form_field: "created",
            form_label: "Fecha Creación",
            form_field_type: "date",
            form_required: true,
            //form_enable: false,
            form_disabled_future: false,
            //form_hide: true

        }, {
            form_field: "icon",
            form_label: "Icono",
            form_field_type: "icon",
        }, {
            form_field: "sorted_menu_opt",
            form_label: "Opciones",
            form_field_type: "draggable",
            //form_enable: false,
            form_options: [{ 'id': 'A', 'primary': 'A' }, { 'id': 'B', 'primary': 'B' }],
        }, {
            form_field: "slider",
            form_label: "Slider",
            form_field_type: "slider",
            //form_props : {'max':500, 'step':10},
            /*form_props : {'sx':{
                display: 'none'
            }}*/
        }
    ]


    const validationSchema = yup.object().shape({
        timestamp: yup
            .string()
            .required("Requerido"),
        currency_id: yup
            .string()
            .required("Requerido"),
        status_id: yup
            .boolean()
            .required("Requerido"),
        color_hex: yup
            .string()
            .required("Requerido"),
        created: yup
            .date()
            .required("Requerido"),
        icon: yup.string(),
    });



    const exampleService = ExampleService()

    const onSubmit = (data) => {
        exampleService.getUsers().then((res) => {
           console.log(res)
        });
        console.log(data)
    }

    const methods = useForm({ resolver: yupResolver(validationSchema), defaultValues: data });

    return (
        <div>
            <Typography variant="body1" color="initial">Login</Typography>
            <Form
                fields={fields}
                formContexMethods={methods}
                onSubmit={onSubmit}
            >
                <Button
                    fullWidth
                    variant="contained"
                    type="submit"
                    sx={{ mt: 3, mb: 2 }}
                >
                    Submit
                </Button>
            </Form>
        </div>
    )
}

export default LoginPage