import React from 'react'
import AuthContext from '../context/AuthContext';
import CrudService from '../services/core/CrudService'

const ExampleService = () => {

  const [, token,] = React.useContext(AuthContext);
  const crudService = CrudService({ endpoint: 'users' })


  const getUsers = () => {
    return crudService._list()
  }

  const createUser = (user) => {
    return crudService._create(user)
  }

  const updateUser = (user_id, user) => {
    return crudService._update(user_id, user)
  }

  const deleteUser = (user_id) => {
    return crudService._delete(user_id)
  }

  const getUser = (user_id) => {
    return crudService._get(user_id)
  }



  return { getUsers, createUser, updateUser, deleteUser, getUser }
}

export default ExampleService