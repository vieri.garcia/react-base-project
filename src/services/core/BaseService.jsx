import { HelpHTTP } from "../../helpers/HelpHTTP";


export const BaseService = () => {

    const source = process.env.REACT_APP_BACKEND_SOURCE
    const request = HelpHTTP()

    const configureRequestOptions = ({token, ...props}) => {
        return {
            headers: {
                //Authorization: "Token " + token
            },
            ...props
        };
    }

    return { source, request, configureRequestOptions }
}
