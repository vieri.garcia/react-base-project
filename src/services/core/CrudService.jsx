import React from 'react'
import AuthContext from '../../context/AuthContext';
import { BaseService } from './BaseService';


const CrudService = ({ endpoint }) => {

    const [, token,] = React.useContext(AuthContext);
    const baseService = BaseService()
    const source = baseService.source;
    const request = baseService.request;
    const options = baseService.configureRequestOptions(token)

    const _create = (body) => {
        options['body'] = body
        return request.post(
            source +
            `/${endpoint}/`,
            options,
        )
            .then((res) => {
                return res;
            });
    }

    const _update = ( id, body) => {
        options['body'] = body
        return request.put(
            source +
            `/${endpoint}/${id}/`,
            options,
        )
            .then((res) => {
               return res;
            });
    }

    const _delete = (id ) => {
        return request.del(
            source +
            `/${endpoint}/${id}/`,
            options,
        )
            .then((res) => {
                return res;
            });
    }

    const _list = ( params= new Object(), extra_url = "" ) => {
        let queryString = new URLSearchParams(params).toString();
        let url = `/${endpoint}${extra_url}`
        if (queryString) {
            url += `?${queryString || ''}`
        }

        return request.get(
            source +
            url,
            options,
        )
            .then(res => {
                return res;
            })
            .catch(e => {
                console.log(e);
            });
    }

    const _get = ( id ) => {
        return request.get(
            source +
            `/${endpoint}/${id}/`,
            options,
        )
            .then((res) => {
                return res
            });
    }

    return { _create, _update, _delete, _list, _get }
}

export default CrudService


