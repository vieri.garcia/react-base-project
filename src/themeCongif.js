import { createTheme } from '@mui/material/styles';
import { deepOrange,lime } from '@mui/material/colors';

const theme = createTheme({
    palette: {
      primary: {
        light: '#52c4bc',
        main: '#00938c',
        dark: '#00655f',
        contrastText: '#000000',
      },
      secondary: {
        light: '#ffff69',
        main: '#f3ea2f',
        dark: '#bcb800',
        contrastText: '#000000',
      },
      text:{
        primary:'#212121',
        secondary:'#616161',
        disabled:'#757466'
      },
      common:{
        black:'#000',
        white:'#fff'
      }
    },
    transitions: {
        easing: {
          // This is the most common easing curve.
          easeInOut: 'cubic-bezier(0.4, 0, 0.2, 1)',
          // Objects enter the screen at full velocity from off-screen and
          // slowly decelerate to a resting point.
          easeOut: 'cubic-bezier(0.0, 0, 0.2, 1)',
          // Objects leave the screen at full velocity. They do not decelerate when off-screen.
          easeIn: 'cubic-bezier(0.4, 0, 1, 1)',
          // The sharp curve is used by objects that may return to the screen at any time.
          sharp: 'cubic-bezier(0.4, 0, 0.6, 1)',
        },
      },
      
    customShadows:{
      z1: '0 1px 2px 0 ${alpha(color, 0.24)}',
      z8: '0 8px 16px 0 ${alpha(color, 0.24)}',
      z12: '0 0 2px 0 ${alpha(color, 0.24)}, 0 12px 24px 0 ${alpha(color, 0.24)}',
      z16: '0 0 2px 0 ${alpha(color, 0.24)}, 0 16px 32px -4px ${alpha(color, 0.24)}',
      z20: '0 0 2px 0 ${alpha(color, 0.24)}, 0 20px 40px -4px ${alpha(color, 0.24)}',
      z24: '0 0 4px 0 ${alpha(color, 0.24)}, 0 24px 48px 0 ${alpha(color, 0.24)}',
      primary: '0 8px 16px 0 ${alpha(palette.primary.main, 0.24)}',
      secondary: '0 8px 16px 0 ${alpha(palette.secondary.main, 0.24)}',
      info: '0 8px 16px 0 ${alpha(palette.info.main, 0.24)}',
      success: '0 8px 16px 0 ${alpha(palette.success.main, 0.24)}',
      warning: '0 8px 16px 0 ${alpha(palette.warning.main, 0.24)}',
      error: '0 8px 16px 0 ${alpha(palette.error.main, 0.24)}'
    }
    
});

export default theme;